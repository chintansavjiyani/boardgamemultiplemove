<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Move extends Model
{
    protected $table='moves';
    protected $fillable=["board_id","piece_id","commands"];
    protected $softDelete = true;




}
