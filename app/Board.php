<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Board extends Model
{
    protected $table='boards';
    protected $fillable=["id","rows","column","game_id"];
    public function game()
    {
        return $this->belongsTo("App\Game");
    }
    public function boardpiece()
    {
        return $this->hasMany("App\BoardPiece");
    }


}
