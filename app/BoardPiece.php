<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BoardPiece extends Model
{
    protected $table= 'board_pieces';
    protected $fillable=["id","board_id","piece_id","x","y","commands","status"];

    public function board()
    {
        return $this->belongsTo("App\Board");
    }
    public function piece()
    {
        return $this->belongsTo("App\Piece");
    }




}
