<?php

namespace App\Http\Controllers;

use App\board;
use App\board_pieces;
use App\BoardPiece;
use App\game;
use App\move;
use App\Piece;
use App\pieces;

use App\User;
use function Faker\Provider\pt_BR\check_digit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class GameController extends Controller
{


    public function index(Request $request)
    {
        $id = [];
        $x = [];
        $y = [];
        $row = $request->input("row");
        $column = $request->input("column");
        $pieces = $request->input("pieces");
        //$command = $request->input("command");
        foreach ($pieces as $piece)
        {
            if($piece["x"] >$row || $piece["y"]>$column)
            {
                $error[]="pieces value must be less then board value";
                return View("game")->with("errors",$error);
            }

        }

        if ($request->has("newGame")) {
            foreach ($pieces as $piece) {
                $piece = Piece::firstOrCreate(['x' => $piece['x'], 'y' => $piece['y']]);
            }
        }
        foreach ($pieces as $piece) {
            $enterpieces = Piece::where("x", $piece["x"])->where("y", $piece["y"])->first();

            $id[] = $enterpieces->id;
            $x[] = $enterpieces->x;
            $y[] = $enterpieces->y;


            $enterpiecesCommand[] = $piece["command"];
        }
       $user= Auth::user();
        $makeGame = Game::create(["isRunning" => 1,"user_id" => $user->id]);
        $currentId = Game::orderBy("id", "desc")->take(1)->first();
        foreach (Game::cursor() as $game) {
            $game::where("id", "!=", $currentId->id)->update(["isRunning" => 0]);
        }
        $board = Board::create(array(
            'rows' => $row,
            'column' => $column,
            'game_id' => $currentId->id

        ));
        $lastBoardId = 0;
        $lastBoard = Board::orderBy("id", "desc")->take(1)->first();
        $lastBoardId = $lastBoard->id;
        for ($i = 0; $i < sizeof($id); $i++) {
            BoardPiece::create([
                "board_id" => $lastBoardId,
                "piece_id" => $id[$i],
                "x" => $x[$i],
                "y" => $y[$i],
                "commands" => $enterpiecesCommand[$i],
                "status" => 1
            ]);
            $lastBoardpieceId = BoardPiece::orderBy("id", "desc")->take(1)->first();
        }
        $newPieces = BoardPiece::where('board_id', $lastBoardId)->get();
        return View("play", [
            "row" => $row,
            "column" => $column,
            "noOfpieces" => sizeof($pieces),
            "pieces" => $newPieces,
            "boardId" => $lastBoardId,
            "piecesIds" => $id,
            "stillRun"=>false

        ]);
    }

    /* public function move(Request $request)
     {
         $error = [];
         $refershFlag = false;
         $user = Auth::user();
         //dd($user);
         $currentGame = $user->game()->where("isRunning", 1)->first();
         if ($currentGame == null) {
             return redirect("/game");
         }
         $last_board = board::where("id", $currentGame->id)->first();
         $newPieces = board_pieces::where('boardId', $last_board->id)->get();
         // dd($newPieces);
         $flag = false;
         $invalidMove = false;
         $row = $last_board->rows;
         $column = $last_board->column;
         $gameid = $last_board->gameId;
         foreach ($newPieces as $piece) {
             $oldValue["x"] = $piece->x;
             $oldValue["y"] = $piece->y;
             $makeOldValues[] = $oldValue;
         }
         $ismove = 0;
         foreach ($newPieces as $piece) {
             $pieceMoves = explode(",", $piece->commands);
             $pieceMove = array_shift($pieceMoves);
             if ($pieceMove) {
                 $ismove++;
             }
           //  dump($piece);

             if (sizeof($pieceMoves) == 1) {
                 board_pieces::where("id", $piece->id)->update(["status" => 0]);
             }
             if ($pieceMove == 'up') {
                 if (!($piece->x == 1)) {
                     $piece->x -= 1;
                     $newCommand = implode(",", $pieceMoves);
                     board_pieces::where('id', $piece->id)->where('boardId', $last_board->id)
                         ->update(['x' => $piece->x, 'y' => $piece->y, 'commands' => $newCommand]);
                     move::create(["boardId" => $last_board->id, "piecesId" => $piece->id, "commands" => $pieceMove]);

                 } else {
                     board_pieces::where('id', $piece->id)->where('boardId', $last_board->id)
                         ->update(['x' => $piece->x, 'y' => $piece->y, 'commands' => implode(",", $pieceMoves)]);
                     //$error[] = "can'y up";
                     $request->session()->flash("error", "can not up");
                     break;
                 }
             } else {
                 if ($pieceMove == 'down') {
                     //dd("false");
                     if ($piece->x == $last_board->rows) {
                         board_pieces::where('id', $piece->id)->where('boardId', $last_board->id)
                             ->update(['x' => $piece->x, 'y' => $piece->y, 'commands' => implode(",", $pieceMoves)]);
                         //  echo '<script language="javascript"> alert("can not move down") </script>';
                        // $error[] = "can't down";
                         $request->session()->flash("error", "can not down");
                         break;
                     }
                     $piece->x += 1;
                     board_pieces::where('id', $piece->id)->where('boardId', $last_board->id)
                         ->update(['x' => $piece->x, 'y' => $piece->y, 'commands' => implode(',', $pieceMoves)]);
                     move::create(["boardId" => $last_board->id, "piecesId" => $piece->id, "commands" => $pieceMove]);


                 } else {
                     if ($pieceMove == "left") {
                         if ($piece->y == 1) {

                             board_pieces::where('id', $piece->id)->where('boardId', $last_board->id)
                                 ->update(['x' => $piece->x, 'y' => $piece->y, 'commands' => implode(",", $pieceMoves)]);

                             // echo '<script language="javascript"> alert("can not move left") </script>';
                             //$error[] = "can't left";
                             $request->session()->flash("error", "can not left");


                             break;
                         }
                         $piece->y -= 1;
                         board_pieces::where('id', $piece->id)->where('boardId', $last_board->id)
                             ->update(['x' => $piece->x, 'y' => $piece->y, 'commands' => implode(',', $pieceMoves)]);
                         move::create(["boardId" => $last_board->id, "piecesId" => $piece->id, "commands" => $pieceMove]);


                     } else {
                         if ($pieceMove == "right") {
                             if (!($piece->y == $last_board->columns)) {
                                 $piece->y += 1;
                                 board_pieces::where('id', $piece->id)->where('boardId', $last_board->id)
                                     ->update([
                                         'x' => $piece->x,
                                         'y' => $piece->y,
                                         'commands' => implode(',', $pieceMoves)
                                     ]);
                                 move::create([
                                     "boardId" => $last_board->id,
                                     "piecesId" => $piece->id,
                                     "commands" => $pieceMove
                                 ]);


                             } else {
                                 board_pieces::where('id', $piece->id)->where('boardId', $last_board->id)
                                     ->update([
                                         'x' => $piece->x,
                                         'y' => $piece->y,
                                         'commands' => implode(",", $pieceMoves)
                                     ]);
                                 // echo '<script language="javascript"> alert("can not move right") </script>';
                                // $error[] = "can't right";
                                 $request->session()->flash("error", "can not right");
                                 break;
                             }

                         } else {
                             $invalidMove = true;
                         }
                     }
                 }
             }


         }
         foreach ($makeOldValues as $makeOldValue) {
             if ($makeOldValue["x"] == $piece->x && $makeOldValue["y"] == $piece->y) {
                 $error[] = "conflict";
                 break;

             }

         }
         $newPieces = board_pieces::where('boardId', $last_board->id)->get();
         if ($ismove != 0) {
             echo " <script>
   setTimeout(function(){location.reload() }, 3000);
       </script>";
         } else {
             $request->session()->flash("error", "game over");
             game::where("id", $currentGame->id)->update(["isRunning" => 0]);

         }

         return view('play',
             ['row' => $row, 'column' => $column, 'gameid' => $gameid, 'pieces' => $newPieces])->with("errors", $error);
     }*/
    public function move()
    {

        $error=[];
        $checkArrays='';
        $stillRun=true;
        $user = Auth::user();
        $game = $user->game()->where("isRunning", 1)->first();
        if($game==null)
        {
            return redirect("/game");
        }
        $board = $game->board()->first();
        $move = move::where("board_id", $board->id)->latest()->first();
        $boardPiece = $board->boardPiece()->where("commands",'!=','')->first();
        if ($move == null) {
            $CheckBoardPiecs = $boardPiece;
        } else {
            $currentBoardpiece = BoardPiece::where("board_id", $move->board_id)->where("piece_id", $move->piece_id)->first();
            $currentBoardpiece->id++;
            $CheckBoardPiecs = BoardPiece::where("id", $currentBoardpiece->id)->where([["board_id",$move->board_id],['commands','!=','']])->first();
        }
        if($CheckBoardPiecs==null)
        {

            $CheckBoardPiecs=$boardPiece;
        }
        $picesX=$CheckBoardPiecs->x;
        $picesY=$CheckBoardPiecs->y;
        dump($picesX);
        dump($picesY);
        $pieceMoves = explode(",", $CheckBoardPiecs->commands);
        $pieceMove = array_shift($pieceMoves);
        $CheckBoardPiecs->commands = implode(",", $pieceMoves);
        if(!in_array($pieceMove,["up","down","right","left",""]))
        {
            $error[]="value should be up,down,right,left";
            $CheckBoardPiecs->commands="";
            $CheckBoardPiecs->save();
            return view("game")->with("errors",$error);
        }

        if ($pieceMove == 'up') {
            if (!($CheckBoardPiecs->x == 1)) {
                $CheckBoardPiecs->x -= 1;
            } else {
                $error []= "can't up";
                $CheckBoardPiecs->commands="";
            }
        } else {
            if ($pieceMove == 'down') {
                if ($CheckBoardPiecs->x == $board->rows) {
                   $error []= "can't down";
                    $CheckBoardPiecs->commands="";
                }else {
                    $CheckBoardPiecs->x += 1;
                }
            } else {
                if ($pieceMove == "left") {
                    if ($CheckBoardPiecs->y == 1) {
                        $error[] = "can't left";
                        $CheckBoardPiecs->commands="";
                    }
                    else {
                        $CheckBoardPiecs->y -= 1;
                    }
                } else {
                    if ($pieceMove == "right") {
                        if (!($CheckBoardPiecs->y == $board->column)) {
                            $CheckBoardPiecs->y += 1;
                        } else {
                            $error[]= "can't right";
                            $CheckBoardPiecs->commands="";
                        }
                    }
                }
            }
        }

    if($pieceMove!=null ) {
        move::create([
            "board_id" => $CheckBoardPiecs->board_id,
            "piece_id" => $CheckBoardPiecs->piece_id,
            "commands" => $pieceMove
        ]);
    }
        $conflictMove=$board->boardPiece()->where("x",$CheckBoardPiecs->x)->where("y",$CheckBoardPiecs->y)->first();

        if($conflictMove==null) {
            $CheckBoardPiecs->save();
        }
        else {
           $oldvalue= $board->boardPiece()->where("piece_id",$CheckBoardPiecs->id)->first();
            $CheckBoardPiecs->commands='';
            $CheckBoardPiecs->save();
            $CheckBoardPiecs->x=$oldvalue->x;
            $CheckBoardPiecs->y=$oldvalue->y;
            $error[]="invalid move";

        }
        $boardPieces = $board->boardPiece()->get();
            foreach ($boardPieces as $boardPiece)
            {

             $checkArrays .=  $boardPiece->commands;

            }
            if($checkArrays=="")
            {
                $error[]="game over,start next game";
                $stillRun=false;
                $game->isRunning=0;
                $game->save();
            }


        $allBoardPiece = $board->boardPiece()->get();
        return view('play',
            ['row' => $board->rows, 'column' => $board->column, 'gameid' => $game->id, 'pieces' => $allBoardPiece,"stillRun"=>$stillRun])
            ->with("errors",$error);

    }

    public function checkAdmin()
    {
        $user= Auth::user();
        if($user->email=="admin@gmail.com")
        {
            return view("admin");
        }
        else
        {
            $error[]="you are not admin";
            return view('game')->with("errors",$error);
        }
    }
}


