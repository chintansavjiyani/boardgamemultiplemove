<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class verfiyController extends Controller
{



    public function verifyEmail(Request $request, $verificationCode)
    {

        //check if verificationCode exists
        if (!$valid = User::where('verification_code', $verificationCode)->first()) {
            return redirect('/login')->withErrors(['that verification code does not exist, try again']);
        }

        $conditions = [
            'verified' => 0,
            'verification_code' => $verificationCode
        ];

        if ($valid = User::where($conditions)->first()) {

            $valid->verified = Carbon::now();
            $valid->save();
//dd($valid->password);
            return redirect('/login')
                ->withInput(['email' => $valid->email,'password'=>$valid->password]);
        }

        return redirect('/home')->with('message', 'Your account is already validated');
    }
}
