<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class FilterVerifiedUsers
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user = Auth::user();
       // dd($user);

        if ($user->verified==0) {
            Auth::logout();
           // $user->sendVerificationEmail();
            return redirect('/login')->with("success","mail sent successfully,check your mail");
        }


        return $next($request);
    }
}
