<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Piece extends Model
{
    protected $table='pieces';
    protected $fillable=["id","x","y"];

public function boardPices()
{
    return $this->hasMany("App\boardPiece");
}


}
