<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $table = 'games';
    protected $fillable = ["id", "isRunning","user_id"];
    public function user()
    {
        return  $this->belongsTo("App\User");
    }
    public function board()
    {
        return $this->hasOne("App\Board");
    }


}
