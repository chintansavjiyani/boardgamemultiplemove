<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoardPiecesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('board_pieces', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("board_id")->unsigned();
            // $table->foreign("boardId")->references('id')->on('game');
            $table->integer("piece_id")->unsigned();
            //$table->integer("picesId")->references('id')->on('pices');
            $table->integer("x");
            $table->integer("y");
            $table->string("commands");
            $table->integer("status");

            $table->timestamps();
        });

    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('board_pieces');
    }
}
