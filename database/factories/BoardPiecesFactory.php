<?php

use Faker\Generator as Faker;

$factory->define(App\BoardPiece::class, function (Faker $faker) {
    $moves=["up","down","right","left"];
    return [
        "commands"=>implode("," ,$faker->randomElements($moves,rand(1,4))),
        "status"=>1
    ];
});
