<?php

use Illuminate\Database\Seeder;

class BoardPiecesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $boards = \App\Board::all();
        foreach ($boards as $board) {
            $noOfPices = rand(1, 4);
            for ($i = 1; $i <= $noOfPices; $i++) {
                $piecesX = rand(1, $board->rows);
                $piecesY = rand(1, $board->column);
                $pieces = \App\Piece::firstOrCreate(["x" => $piecesX, "y" => $piecesY]);

                // dump($pices);
                $conflictBoardPices = \App\BoardPiece::where("board_id", $board->id)->where("piece_id",
                    $pieces->id)->first();
                if ($conflictBoardPices == null) {
                    factory(App\BoardPiece::class)->create([
                        "board_id" => $board->id,
                        "piece_id" => $pieces->id,
                        "x" => $pieces->x,
                        "y" => $pieces->y
                    ]);
                }
            }
        }
    }

}
