<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Auth;

class BoardTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users=\App\User::get();

        foreach ($users as $user) {
            $games[] = \App\Game::where('user_id', $user->id)->get();
        }

        for($i=0;$i<sizeof($games);$i++) {

            foreach ($games[$i] as $game) {
                $boards = factory(App\Board::class)->create(['game_id' => $game->id]);
            }
        }
    }
}
