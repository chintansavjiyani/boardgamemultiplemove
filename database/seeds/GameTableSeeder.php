<?php

use Illuminate\Database\Seeder;

class GameTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users= \App\User::all();
        // $user=  Auth::user();
        //  dd($user->id);
        foreach ($users as $user) {
            factory(App\Game::class, 2)->create(['user_id' => $user->id]);
        }
    }
}
