<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('welcome');
});


Route::get('/game', function () {
    return view('game');
})->middleware("auth")->middleware("verified");

Route::post('/controller','gameController@index')->name('gameController')->middleware("auth")->middleware("verified");
Route::any('/move','GameController@move')->name('move')->middleware("auth")->middleware("verified");

Route::any('/play', function () {
    return view('play')->name("play");
})->middleware("auth")->middleware("verified");
Route::any('/adminPannel', function () {
    return view('admin')->name("adminPannel");
})->middleware("auth")->middleware("verified");
Auth::routes();

Route::any("/admin",'GameController@checkAdmin')->name("admin");
Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');

Route::get('verifyemail', function(){
    return View("checkmail");
});
Route::get('user/verifyemail/{code}', 'verfiyController@verifyEmail');

